# horoscopia | version-1

## description

>Who needs professional counseling when you can just look at the stars! By providing your birthday, we'll determine your zodiac sign and provide you with the insight and guidance of the universe.
>
>This project is a mobile-first React app that utilizes the API [AstrologyAPI](https://www.astrologyapi.com/) on a backend Express server to provide predictions based on your birthday.

## setup 
- clone project
- install **npm** and **nodejs** if not installed:   https://nodejs.org/en/download/
- run `npm install` in project directory to install dependencies
- run `npm run build` to build project for express server
- obtain a premium account (14 day trial) from astrologyapi:  https://www.astrologyapi.com/
- create `.env` in project directory containing this:
>>> `ASTROLOGY_API_USER_ID=[YOUR_USER_ID]` 
>>>
>>> `ASTROLOGY_API_KEY=[YOUR_KEY]`

## usage
* run `npm start` in project directory to run server at `localhost:8080`

## development
* have npm dependencies **nodemon** and **concurrently** installed globally if not already by running the following:
>>> `npm install -g nodemon`
>>>
>>> `npm install -g concurrently`
* run `npm run dev` to run server and watch for changes
* two servers are ran, one at `localhost:3000` and one at `localhost:8080`. the one at port 3000 opens by default and will update with changes to the code. the one on 8080 is for ajax requests to the backend  and will only update upon `npm build`. it's recommended that you just use the one at port 3000.