// imports
const express = require('express');
const path = require('path');
const cors = require('cors');
const astrology = require('./server/astrology-sdk');

// defines a port and express app to use
const port = process.env.PORT || 8080;
const app = express();

// enabling middleware
app.use(express.static(path.join(__dirname)));
app.use(express.static(path.join(__dirname, 'build')));
app.use(cors());

// this route retrieves predictions based on the sign from the api and returns the json
app.get('/predictions/:sign', (request, response) => 
{
    // makes a call with the api's sdk
    astrology.call(`sun_sign_prediction/daily/${request.params.sign}/`, '', '', '', '', '', '', '', '5.5', (error, astrologyResponse) => 
    {

        if(error || !astrologyResponse || !JSON.parse(astrologyResponse).prediction)
        {
            console.log('failed to retrieve data from astologyapi.com');

            if(error)
            {
                console.error(error);
            }

            response.status(500);
            return response.send('failed to retrieve data from astrologyapi.com');
        }

        let predictions = [];

        let predictionData = JSON.parse(astrologyResponse).prediction;
        let predictionSubjects = Object.keys(predictionData);
        
        // the first prediction tends to be a little less pg so it's hidden in the back instead
        predictionSubjects = predictionSubjects.reverse();

        // reformats the data so page turning through predictions are easy on the front end
        for(let subject of predictionSubjects)
        {
            let content = predictionData[subject];

            // reformats prediction subjects to something i'd want to show to the user
            while(subject.includes('_'))
            {
                subject = subject.replace('_', '-');
            }
            let prediction =
            {
                subject,
                content,
            }
            predictions.push(prediction);
        }

        response.json(predictions);
    });
});

// all paths not explicity defined above are handled by the react app and its routing
app.get('/*', (request, response) =>
{
    response.sendFile(path.join(__dirname, 'build', 'index.html'));
});

// start app on given port
app.listen(port, () => 
{
    console.log('express server running');
});