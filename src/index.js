
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

// routes
import App from './App';
import Credits from './routes/Credits';
import NotFound from './routes/NotFound';
import ServerError from './routes/ServerError';

ReactDOM.render(
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={App}/>
        <Route exact path="/credits" component={Credits} />
        <Route exact path="/server-error" component={ServerError} />
        <Route exact path='*' component={NotFound} />
      </Switch>
    </BrowserRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
