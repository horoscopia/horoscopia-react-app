import React, { Component } from 'react';

import '../styles/NotFound.css';

// not found page for non-existing route
// react app defaults to this route if all others fail

export default class NotFound extends Component
{
    render()
    {
        return (
            <div className="not-found">
                <h1>404</h1>
                <h2>not found</h2>
                <p className="message">this page simply doesn't exist</p>
                <a href="/" className="link-home">
                    <p>return home</p>
                </a>
            </div>
        );
    }
}