import React, { Component } from 'react';

import '../styles/Credits.css';

// credits third-party sources used on this site

export default class Credits extends Component
{
    render()
    {
        return (
            <div className="credits">
                <h1>credits</h1>
                <section id="section-astrology-api">
                    <h2><a target="_blank" rel="noopener noreferrer" href="https://www.astrologyapi.com/">astrology-api</a></h2>
                    <p>horoscope predictions provided via <a target="_blank" rel="noopener noreferrer" href="https://www.astrologyapi.com/">astrology api</a></p>
                </section>
                <section id="section-pexels">
                    <h2><a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/">pexels</a></h2>
                    <p>backgrounds obtained from <a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/">pexels</a> with artists linked below</p>
                    <div id="pexels-links">
                        <span><a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/@miriamespacio">miriamespacio</a></span>
                        <span><a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/@igor-haritanovich-814387">igor-haritanovich</a></span>
                        <span><a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/@freeimages9">icon0com</a></span>
                        <span><a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/@olly">andrea-piacquadio</a></span>
                        <span><a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/@fox-58267">fox</a></span>
                        <span><a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/@pixabay">pixabay</a></span>
                        <span><a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/@gratisography">gratisography</a></span>
                        <span><a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/@mathew-thomas-318779">mathew-thomas</a></span>
                        <span><a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/@scottwebb">scott-webb</a></span>
                        <span><a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/@chalo-garcia-2092235">chalo-garcia</a></span>
                        <span><a target="_blank" rel="noopener noreferrer" href="https://www.pexels.com/@markusspiske">markus-spiske</a></span>
                    </div>
                </section>
                <section id="section-flaticon">
                    <h2><a target="_blank" rel="noopener noreferrer" href="https://www.flaticon.com/">flaticon</a></h2>
                    <p>icons obtained from <a target="_blank" rel="noopener noreferrer" href="https://www.flaticon.com/packs/zodiac">zodiac</a> and <a target="_blank" rel="noopener noreferrer" href="https://www.flaticon.com/packs/space-141">space</a> pack by <a target="_blank" rel="noopener noreferrer" href="https://www.flaticon.com/authors/freepik">freepik</a></p>
                </section>
                <section id="section-footer">
                    <p>
                        <a href="/">return home</a>
                    </p>
                </section>
            </div>
        );
    }
}