import React, { Component } from 'react';

import '../styles/ServerError.css';

// not found page for non-existing route
// react app defaults to this route if all others fail

export default class NotFound extends Component
{
    render()
    {
        return (
            <div className="server-error">
                <h1>500</h1>
                <h2>server error</h2>
                <p className="message">something went wrong on our end</p>
                <a href="/" className="link-home">
                    <p>return home</p>
                </a>
            </div>
        );
    }
}