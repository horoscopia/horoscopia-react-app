import React, { Component } from 'react';

// this is the footer of the site
// it gives the copyright and links to the /credits page

export default class Header extends Component
{
    render()
    {
        return (
            <footer className={`footer stage-${this.props.stage}`}>
                <span>&copy; theta ultd | 2020</span>
                <a href="/credits"><span>view credits</span></a>
            </footer>
        );
    }
} 