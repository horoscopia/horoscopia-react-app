import React, { Component } from 'react';

// this is the component that displays the horoscope predictions

export default class Results extends Component
{
    // turns page forward to the next prediction
    turnPage = () =>
    {
        const numberOfPages = this.props.predictions.length;

        let predictionIndexToView = this.props.predictionIndexToView;

        if(predictionIndexToView + 1 >= numberOfPages)
        {
            predictionIndexToView = 0;
        }
        else
        {
            predictionIndexToView++;
        }

        this.props.setPredictionIndexToView(predictionIndexToView);
    }

    // turns page forward to the back prediction
    turnBackPage = () =>
    {
        const numberOfPages = this.props.predictions.length;

        let predictionIndexToView = this.props.predictionIndexToView;

        if(predictionIndexToView <= 0)
        {
            predictionIndexToView = numberOfPages - 1;
        }
        else
        {
            predictionIndexToView--;
        }

        this.props.setPredictionIndexToView(predictionIndexToView);
    }

    render()
    {
        // organizes all the horoscope data to display to the user
        const sign = this.props.sign;
        const predictions = this.props.predictions;
        const predictionIndexToView = this.props.predictionIndexToView;
        const predictionToView = predictions[predictionIndexToView];

        let pageNumber = predictionIndexToView + 1;
        let numberOfPages = predictions.length;

        let signSrc = '';

        // changes the background based on the zodiac sign of the user
        if(sign !== 'unchosen')
        {
            signSrc = `/resources/zodiac-signs/sign-${sign}.svg`;
        }

        return (
            <div className={`results stage-${this.props.stage}`}>
                <h2>{sign}</h2>
                <img className="sign" src={signSrc} alt=""/>
                <h3 className="subject">{predictionToView.subject}</h3>
                <p className="content">{predictionToView.content}</p>
                <div className="divider"></div>
                <p className="page-number">{pageNumber}/{numberOfPages}</p>
                <span className="previous" onClick={this.turnBackPage}>&lt;</span>
                <span className="next" onClick={this.turnPage}>&gt;</span>
            </div>
        );
    }
}