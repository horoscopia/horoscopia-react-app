import React, { Component } from 'react';

// this is the component that determines the users birthday and
// request results based on them

export default class DateSelection extends Component
{
    // the max amount of days in a month
    // used to create calendar day selection
    dayCounts = 
    {
        Jan: 31,
        Feb: 29,
        Mar: 31,
        Apr: 30,
        May: 31,
        Jun: 30,
        Jul: 31,
        Aug: 31,
        Sep: 30,
        Oct: 31,
        Nov: 30,
        Dec: 31,
    }

    // month and day stored in state to change display
    // based on what the user already selected
    state = 
    {
        month: 'unchosen',
        day: 'unchosen',
    }

    // handles when the user selects a month
    selectMonth = (event) =>
    {
        let element = event.target;
        let month = element.innerHTML;
        let validMonths = Object.keys(this.dayCounts);

        // ensures that selected month is an actual month
        // and updates the state
        if(validMonths.includes(month))
        {
            this.setState(previousState => 
            {
                return { month };
            });
        }
    }

    // handles when the user selects a day
    selectDay = (event) =>
    {
        let element = event.target;
        let day = element.innerHTML;
        
        // makes sure selected day isn't larger than the amount of days in
        // a given month
        if(this.dayCounts[this.state.month] >= parseInt(day))
        {
            // proceeds to the transition then results stages of the application with
            // the users input
            this.setState(previousState => 
                {
                    // calls up to retrieve the predictions from the server and saves the promise
                    let httpRequestPromise = this.props.retrievePredictions(this.state.month, parseInt(day));

                    // adds one callback to the promise to reset the state upon request being received
                    // this will ensure that if the user goes back they will see the date selection stage
                    // without any input given
                    httpRequestPromise.then(() => 
                    {
                        setTimeout(() => 
                        {
                            this.setState(previousState => 
                            {
                                return {
                                    month: 'unchosen',
                                    day: 'unchosen',
                                }
                            });
                        }, 2000);
                    });

                    return { day };
                });
        }
    }

    // this handler is used to go from the day selection phase
    // back to the month selection phase in case the user
    // selected the wrong month
    goBack = () =>
    {
        this.setState(previousState => 
        {
            return {
                month: 'unchosen',
            };
        });
    }

    // this creates all the needed day selectors for a given month
    // based on how many days are in that month
    constructDaySelectors()
    {
        if(this.state.month === 'unchosen')
        {
            return [];
        }
        
        let dayCount = this.dayCounts[this.state.month];
        let daySelectors = [];

        for(let i = 0; i < dayCount; i++)
        {
        let daySelector = <span key={`${this.state.month}-${i + 1}`} className="day-selector" type="button" onClick={this.selectDay}>{i + 1}</span>;

            daySelectors.push(daySelector);
        }
        return daySelectors;
    }

    render()
    {
        let content;

        // checks if the month has already been chosen and if not adds month selection content
        if(this.state.month === 'unchosen')
        {
            let monthSelectors = [
                <span key="Jan" className="month-selector" type="button" onClick={this.selectMonth}>Jan</span>,
                <span key="Feb" className="month-selector" type="button" onClick={this.selectMonth}>Feb</span>,
                <span key="Mar" className="month-selector" type="button" onClick={this.selectMonth}>Mar</span>,
                <span key="Apr" className="month-selector" type="button" onClick={this.selectMonth}>Apr</span>,
                <span key="May" className="month-selector" type="button" onClick={this.selectMonth}>May</span>,
                <span key="Jun" className="month-selector" type="button" onClick={this.selectMonth}>Jun</span>,
                <span key="Jul" className="month-selector" type="button" onClick={this.selectMonth}>Jul</span>,
                <span key="Aug" className="month-selector" type="button" onClick={this.selectMonth}>Aug</span>,
                <span key="Sep" className="month-selector" type="button" onClick={this.selectMonth}>Sep</span>,
                <span key="Oct" className="month-selector" type="button" onClick={this.selectMonth}>Oct</span>,
                <span key="Nov" className="month-selector" type="button" onClick={this.selectMonth}>Nov</span>,
                <span key="Dec" className="month-selector" type="button" onClick={this.selectMonth}>Dec</span>,
            ];

            content = ( 
                <div className="month-selection">
                    <h3>What month were you born?</h3>
                    <div className="month-selectors">
                        { monthSelectors } 
                    </div>
                </div>
            );
        }
        // checks if the day has already been chosen and if not adds day selection content
        else if(this.state.day === 'unchosen')
        {
            let daySelectors = this.constructDaySelectors();

            content = ( 
                <div className="day-selection">
                    <h3>Alright, what day?</h3>
                    <div className="day-selectors">
                        { daySelectors } 
                    </div>
                    <p className="back" onClick={this.goBack}>change month</p>
                </div>
            );
        }
        // if everything has been selected the transition message is displayed to the user
        else
        {
            content = ( 
                <span className="message-transition">determining clearly reliable predictions...</span>
            );
        }

        // the content inside is determined by the logic above
        return (
            <div className={`date-selection stage-${this.props.stage}`}>
                { content }
            </div>
        );
    }
}