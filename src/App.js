import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import './styles/App.css';

import Header from './components/Header';
import DateSelection from './components/DateSelection';
import Results from './components/Results';
import Footer from './components/Footer';

// main component representing the app
// cycles through three stages (selection, transition, results)
// and hides certain components based on the stage

export default class App extends Component
{
	// state stores predictions retrieved from the server, the zodiac
	// sign of the user, the stage the app is in, and index of the prediction
	// the user is viewing in the results (page turning stuff)
	state =
	{
		predictions: [ { subject: '', content: '' } ],
		sign: 'unchosen',
		stage: 'selection',
		predictionIndexToView: 0,
	}

	// takes the month and day and figures out the user's zodiac sign
	determineSign(month, day)
	{
		if((month === 'Mar' && day >= 21) || (month === 'Apr' && day <= 19))
		{
			return 'aries'
		}
		else if((month === 'Apr' && day >= 20) || (month === 'May' && day <= 20))
		{
			return 'taurus';
		}
		else if((month === 'May' && day >= 21) || (month === 'Jun' && day <= 20))
		{
			return 'gemini';
		}
		else if((month === 'Jun' && day >= 21) || (month === 'Jul' && day <= 22))
		{
			return 'cancer';
		}
		else if((month === 'Jul' && day >= 23) || (month === 'Aug' && day <= 22))
		{
			return 'leo';
		}
		else if((month === 'Aug' && day >= 23) || (month === 'Sep' && day <= 22))
		{
			return 'virgo';
		}
		else if((month === 'Sep' && day >= 23) || (month === 'Oct' && day <= 22))
		{
			return 'libra';
		}
		else if((month === 'Oct' && day >= 23) || (month === 'Nov' && day <= 21))
		{
			return 'scorpio';
		}
		else if((month === 'Nov' && day >= 22) || (month === 'Dec' && day <= 21))
		{
			return 'sagittarius';
		}
		else if((month === 'Dec' && day >= 22) || (month === 'Jan' && day <= 19))
		{
			return 'capricorn';
		}
		else if((month === 'Jan' && day >= 20) || (month === 'Feb' && day <= 18))
		{
			return 'aquarius';
		}
		else if((month === 'Feb' && day >= 19) || (month === 'Mar' && day <= 20))
		{
			return 'pisces';
		}
	}

	// makes a call to the server to retrieve prediction data from the api

	retrievePredictions = (month, day) =>
	{
		let sign = this.determineSign(month, day);

		let promise = axios({
			method: 'get',
			url: `predictions/${sign}`,
			headers: {
				'Content-Type': 'application/json'
			}
		});
		
		promise.then(response => 
		{
			let predictions = response.data;

			// updates the state to store prediction data and 
			// sets the stage to 'transition' to display the 
			// transition to the results
			this.setState({ 
				predictions,
				sign,
				stage: 'transition',
			});
			// sets a timeout giving the transition enough time
			// and then proceeding to display the results
			setTimeout(() => 
			{
				this.setState({ 
					stage: 'results',
				});

			}, 1500);
			
		}).catch(error => 
		{
			this.setState({
				serverError: true,
			});

			console.log('failed to retrieve horoscope from server');
			console.error(error);
		});

		return promise;
	}

	// handles a page turn in the results stage
	setPredictionIndexToView = (index) =>
	{
		let numberOfPredictions = this.state.predictions.length;

		// ensures the the new page actually exists before changing to it
		if(index >= 0 && index < numberOfPredictions)
		{
			this.setState(previousState => 
			{
				return {
					predictionIndexToView: index,
				}
			});
		}
	}

	// handles a reset from the results stage back
	// to the selection stage
	reset = () =>
	{
		this.setState(previousState => 
		{ 
			return { 
				stage: 'transition',
			}
		});
		setTimeout(() => 
		{
			this.setState(previousState => 
			{ 
				return { 
					predictions: [ { subject: '', content: '' } ],
					sign: 'unchosen',
					stage: 'selection',
					predictionIndexToView: 0,
				}
			});

		}, 2000);
	}

	render()
	{

		if(this.state.serverError)
		{
			return <Redirect to='/server-error' />
		}

		const stage = this.state.stage;

		let zodiacBackgroundStyle = '';

		// if the sign has been determined the background for the results is set
		if(this.state.sign !== 'unchosen')
		{
			zodiacBackgroundStyle = `url('/resources/zodiac-backgrounds/background-${this.state.sign}.jpg')`
		}

		// throughout the jsx the 'stage' of the app is provided as a prop
		// to various components to communicate whether they should 
		// display themselves, hide, or transition
		return (
			<div className="app">
				<div className={`constellation stage-${stage}`}></div>
				<div className={`zodiac-background stage-${stage}`} style={{ backgroundImage: zodiacBackgroundStyle }}></div>
				<div className="divider"></div>
				<Header stage={stage} />
				<DateSelection stage={stage} retrievePredictions={this.retrievePredictions}/>
				<Results stage={stage} sign={this.state.sign} predictions={this.state.predictions} predictionIndexToView={this.state.predictionIndexToView} setPredictionIndexToView={this.setPredictionIndexToView}/>
				<div className={`reset stage-${stage}`} onClick={this.reset}>reset</div>
				<Footer stage={stage}/>
			</div>
		);
	}
}
